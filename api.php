<?php
session_start();

include "./lib/game.php";

if ($_POST["action"] == 'new_game') {
    $game = startGame($_POST["playerCount"]);
    setGame($game);
    echo json_encode($game->getState());
} elseif ($_POST["action"] == 'roll') {
    $game = getGame();
    $game->roll();
    setGame($game);
    echo json_encode($game->getState());
} elseif ($_POST["action"] == 'tick') {
    $game = getGame();
    $game->tick();
    setGame($game);
    echo json_encode($game->getState());
} elseif ($_POST["action"] == 'get') {
    $game = getGame();
    echo json_encode($game->getState());
}

function getGame()
{
    if (!empty($_SESSION['game'])) {
        return unserialize($_SESSION['game']);
    } else {
        return false;
    }
}

function setGame($game)
{
    $_SESSION['game'] = serialize($game);
}

function startGame($playerCount)
{
    $game = new Game();
    $game->build(6, $playerCount);
    return $game;
}

function endGame()
{
    $_SESSION['game'] = null;
    unset($_SESSION['game']);
    $_SESSION['step'] = 'end_game';
}

<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game</title>

    <link href="css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">

    <script src="js/vendor/bootstrap.bundle.min.js"></script>
    <script src="js/vendor/jquery-3.6.0.min.js"></script>
    <script src="js/vendor/fabric.min.js"></script>
    <script src="js/game.js"></script>
</head>

<body>
    <div class="container">
        <div class="header">
            <h1>Snakes & Ladders</h1>
        </div>
        <br>
        <div class="game-steps">
            <div class="step step-new-game">
                <h2>Start new game</h2>
                <div class="row g-3">
                    <div class="col-auto">
                        <label for="game-player-count" class="visually-hidden">Players</label>
                        <input type="number" min="2" max="4" placeholder="Players" class="form-control" id="game-player-count" style="width: 200px">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-primary mb-3 new-game-submit">Start</button>
                    </div>
                </div>
            </div>


            <div class="step step-in-game">
                <canvas id="game-canvas" width="600" height="600"></canvas>
                <br>
                <div class="info">
                    <span class="game-current-player-no">Current Player: <span>1</span></span> ||
                    <span class="game-current-player-roll">Rolled: <span>-</span></span>
                </div>
                <div class="actions">
                    <button class="btn btn-primary mb-3 game-roll">Roll</button>
                    <button class="btn btn-primary mb-3 game-next" disabled>Next</button>
                </div>
            </div>

            <div class="step step-end-game">
                <h2>Congratulations! Player <span>-</span> is the winner!</h2>
            </div>
        </div>
    </div>
</body>

</html>
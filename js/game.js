function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const CONSTANTS = {
  BOARD_SIZE: 6,
  CELL_SIZE: 100,
  LADDER_COLOR: "rgb(0,255,0, 0.4)",
  SNAKE_COLOR: "rgb(255,0,0, 0.4)",
  PLAYER_COLORS: [
    "#cf9d31",
    "#fd8548",
    "#414bc4",
    "#4b7d4c",
    "#c06518",
    "#ee45d1",
  ],
  GAME_STEPS: {
    NEW_GAME: 1,
    IN_GAME: 2,
    END_GAME: 3,
  },
  API_URL: "http://localhost:8080/api.php",
};

// convert api state to game state
const apiStateToGameState = (apiState) => {
  const gameState = {
    playerCount: parseInt(apiState.playerCount),
    ladders: Object.keys(apiState.board.ladders).map((from) => ({
      from,
      to: apiState.board.ladders[from],
    })),
    snakes: Object.keys(apiState.board.snakes).map((from) => ({
      from,
      to: apiState.board.snakes[from],
    })),
    winnerPlayerNo: apiState.winningPlayer,
    playerPositions: apiState.player.reduce(
      (acc, player, playerNo) => ({ ...acc, [playerNo]: player.position }),
      {}
    ),
    currentPlayer: {
      playerNo: apiState.turn.player,
      roll: apiState.turn.roll,
      position: apiState.turn.position,
    },
  };

  return gameState;
};

// ajax communication with the server
const api = () => {
  const request = async (action, data = {}) => {
    const res = await $.ajax({
      url: CONSTANTS.API_URL,
      type: "POST",
      data: { ...data, action },
    });

    try {
      return JSON.parse(res);
    } catch (e) {
      throw new Error("Can't communicate with the server");
    }
  };

  const get = () => request("get");

  const newGame = (data) => request("new_game", data);
  const roll = () => request("roll");
  const tick = () => request("tick");

  return { get, newGame, roll, tick };
};

const buildGrid = () => {
  let count = 36;
  const gridCells = [];

  for (let i = 0; i < CONSTANTS.BOARD_SIZE; i++) {
    for (let j = 0; j < CONSTANTS.BOARD_SIZE; j++) {
      const rect = new fabric.Rect({
        width: CONSTANTS.CELL_SIZE,
        height: CONSTANTS.CELL_SIZE,
        stroke: "rgb(178,204,255)",
        strokeWidth: 2,
        fill: null,
        originX: "center",
        originY: "center",
      });

      const text = new fabric.Text(
        `${count === 1 ? "Start (1)" : count === 36 ? "End (36)" : count + ""}`,
        {
          fontFamily: "Calibri",
          fill: "#fff",
          fontSize: 20,
          textAlign: "center",
          originX: "center",
          originY: "center",
        }
      );

      const group = new fabric.Group([rect, text], {
        top: i * CONSTANTS.CELL_SIZE,
        left: j * CONSTANTS.CELL_SIZE,
      });

      gridCells[count - 1] = group;

      count -= 1;
    }
  }

  return gridCells;
};

const buildPolygonCellSpan = (
  from,
  to,
  { color = "#fff", thick = 10 } = {}
) => {
  const fromCell = from;
  const toCell = to;
  const fromCenter = { x: fromCell.left + 10, y: fromCell.top + 10 };
  const toCenter = { x: toCell.left + 10, y: toCell.top + 10 };

  const line = new fabric.Line(
    [fromCenter.x, fromCenter.y, toCenter.x, toCenter.y],
    {
      fill: color,
      stroke: color,
      strokeWidth: 16,
    }
  );
  return line;
};

const buildLadder = (from, to) => {
  return buildPolygonCellSpan(from, to, {
    color: CONSTANTS.LADDER_COLOR,
    thick: 20,
  });
};

const buildSnake = (from, to) => {
  return buildPolygonCellSpan(from, to, {
    color: CONSTANTS.SNAKE_COLOR,
    thick: 20,
  });
};

const buildPlayer = (startCell, playerNo, color) => {
  const cercle = new fabric.Circle({
    radius: 12,
    fill: color,
    stroke: "white",
    strokeWidth: 3,
    originX: "center",
    originY: "center",
  });
  const text = new fabric.Text("" + playerNo, {
    fontFamily: "Calibri",
    fill: "#fff",
    fontSize: 15,
    textAlign: "center",
    originX: "center",
    originY: "center",
  });

  const player = new fabric.Group([cercle, text], {
    left: startCell.left - 12 + rand(12, 88),
    top: startCell.top - 12 + rand(12, 88),
  });

  return player;
};

const draw = (canvas) => {
  let elements = {
    gridCells: [],
    laders: [],
    snakes: [],
    players: [],
  };

  const drawGrid = () => {
    canvas.setBackgroundColor(
      {
        source: "http://fabricjs.com/assets/escheresque_ste.png",
      },
      canvas.renderAll.bind(canvas)
    );
    const gridCells = buildGrid();
    gridCells.forEach((cell) => canvas.add(cell));
    elements.gridCells = gridCells;
    return gridCells;
  };

  const drawLadder = (from, to) => {
    if (!elements.gridCells || !elements.gridCells.length) {
      throw new Error("Grid not initialized");
    }

    const ladder = buildLadder(
      elements.gridCells[from],
      elements.gridCells[to]
    );
    elements.laders.push(ladder);

    canvas.add(ladder);

    return ladder;
  };

  const drawSnake = (from, to) => {
    if (!elements.gridCells || !elements.gridCells.length) {
      throw new Error("Grid not initialized");
    }

    const snake = buildSnake(elements.gridCells[from], elements.gridCells[to]);
    elements.snakes.push(snake);

    canvas.add(snake);

    return snake;
  };

  const drawPlayer = (playerNo) => {
    if (!elements.gridCells || !elements.gridCells.length) {
      throw new Error("Grid not initialized");
    }
    if (elements.players[playerNo]) {
      throw new Error("Player already initialized");
    }
    const player = buildPlayer(
      elements.gridCells[0],
      playerNo + 1,
      CONSTANTS.PLAYER_COLORS[playerNo]
    );
    elements.players.push(player);

    canvas.add(player);

    return player;
  };

  // have a getter in order to return the latest objects - reference might be lost at some point
  const getElements = () => elements;

  const reset = () => {
    elements = {
      gridCells: [],
      laders: [],
      snakes: [],
      players: [],
    };
  };

  return {
    grid: drawGrid,
    snake: drawSnake,
    ladder: drawLadder,
    player: drawPlayer,
    getElements,
    reset,
  };
};

const board = (canvas, state) => {
  const _draw = draw(canvas);
  let _isInitialized = false;
  let _state = null;

  // clone the state to prevent unwanted mutations
  const setState = (state) => (_state = JSON.parse(JSON.stringify(state)));

  setState(state);

  // add everyhing to the canvas (grid, ladders, snakes and players)
  const initialize = (state) => {
    if (_isInitialized) throw new Error("Board already initialized");

    setState(state);

    _draw.grid();

    _state.ladders.forEach((ladder) => _draw.ladder(ladder.from, ladder.to));

    _state.snakes.forEach((snake) => _draw.snake(snake.from, snake.to));

    _state.playerPositions = {};
    for (let playerNo = 0; playerNo < _state.playerCount; playerNo++) {
      _draw.player(playerNo);
      _state.playerPositions[playerNo] = 0;
    }

    _isInitialized = true;
  };

  const update = (state) => {
    if (!_isInitialized) throw new Error("Board not initialized");

    Object.keys(state.playerPositions).forEach((playerNo) => {
      const playerPosition = state.playerPositions[playerNo];

      // update player position
      const cell = _draw.getElements().gridCells[playerPosition];
      _draw.getElements().players[playerNo].left =
        cell.left - 12 + rand(12, 88);
      _draw.getElements().players[playerNo].top = cell.top - 12 + rand(12, 88);
      _draw.getElements().players[playerNo].setCoords();

      // highlight current player - not working
      if (state.currentPlayer.playerNo == parseInt(playerNo)) {
        _draw.getElements().players[playerNo].set({ strokeWidth: 7 });
      }
    });

    canvas.renderAll();

    setState(state);
  };

  const reset = () => {
    _draw.reset();
    // clear the canvas
    canvas.clear();
  };

  const getState = () => _state;

  return { initialize, update, getState, reset };
};

const game = (elements, canvas, state) => {
  let _state = null;
  let _isInitialized = false;

  // clone the state to prevent unwanted mutations
  const setState = (state) => (_state = JSON.parse(JSON.stringify(state)));

  // initialize the state
  setState(state);

  const _board = board(canvas, state);
  const _api = api();

  const initialize = () => {
    // only allow if game uninitialized
    if (_isInitialized) throw new Error("Game already initialized");

    // toggle the last screen containg the new game form - this is to ensure same behaviour regardless of css initial values
    elements.newGameContainer.show();
    elements.inGameContainer.hide();
    elements.endGameContainer.hide();

    // set initial step state
    _state.step = CONSTANTS.GAME_STEPS.NEW_GAME;

    _isInitialized = true;
  };

  const newGame = async (playerCount) => {
    // only allow if game initialized
    if (_isInitialized) throw new Error("Game not initialized");
    if (_state.step !== CONSTANTS.GAME_STEPS.NEW_GAME)
      throw new Error("Game already started");

    // toggle the last screen containg the game itself
    elements.newGameContainer.hide();
    elements.inGameContainer.show();
    elements.endGameContainer.hide();

    _state.playerCount = playerCount;
    _state.step = CONSTANTS.GAME_STEPS.IN_GAME;

    // update game state with state from server
    const res = await _api.newGame({ playerCount: _state.playerCount });
    const newState = apiStateToGameState(res);
    setState({ ..._state, ...newState });
  };

  const endGame = () => {
    // only allow if game initialized
    if (_isInitialized) throw new Error("Game not initialized");
    if (_state.step !== CONSTANTS.GAME_STEPS.IN_GAME)
      throw new Error("Game not started");

    // toggle the last screen containg the winner info
    elements.newGameContainer.hide();
    elements.inGameContainer.hide();
    elements.endGameContainer.show();

    // disable everything else
    elements.rollButton.attr("disabled", true);
    elements.nextButton.attr("disabled", true);

    _state.step = CONSTANTS.GAME_STEPS.END_GAME;

    // update with winning player info
    elements.endGamePlayerNoText.html(_state.winnerPlayerNo + 1);
  };

  const start = () => {
    // only allow if game initialized
    if (_isInitialized) throw new Error("Game not initialized");
    if (_state.step !== CONSTANTS.GAME_STEPS.IN_GAME)
      throw new Error("Game not started");

    // clear the canvas
    _board.reset();
    // initialize the board
    _board.initialize(_state);

    // backpropagate state from board
    _state = setState(_board.getState());
  };

  const roll = async () => {
    // only allow if game initialized
    if (_isInitialized) throw new Error("Game not initialized");
    if (_state.step !== CONSTANTS.GAME_STEPS.IN_GAME) {
      throw new Error("Game not started");
    }

    // update game state with state from server
    const res = await _api.roll();
    const newState = apiStateToGameState(res);
    setState({ ..._state, ...newState });

    _board.update(_state);

    // only allow ending the turn and moving to next player
    elements.rollButton.attr("disabled", true);
    elements.nextButton.removeAttr("disabled");

    // update with current dice roll value
    elements.currentPlayerRollText.html(_state.currentPlayer.roll);

    // check if game ended
    if (_state.winnerPlayerNo !== null) endGame();
  };

  const tick = async () => {
    if (_isInitialized) throw new Error("Game not initialized");
    if (_state.step !== CONSTANTS.GAME_STEPS.IN_GAME) {
      throw new Error("Game not started");
    }

    // update game state with state from server
    const res = await _api.tick();
    const newState = apiStateToGameState(res);
    setState({ ..._state, ...newState });

    _board.update(_state);

    // only allow rolling the dice
    elements.rollButton.removeAttr("disabled");
    elements.nextButton.attr("disabled", true);

    // update with current player info
    elements.currentPlayerNoText.html(_state.currentPlayer.playerNo + 1);

    // check if game ended
    if (_state.winnerPlayerNo !== null) endGame();
  };

  const getState = () => _state;

  return { initialize, newGame, start, roll, tick, getState };
};

// always get a fresh object
const getElements = () => {
  const elements = {
    newGameContainer: $(".step-new-game"),
    inGameContainer: $(".step-in-game"),
    endGameContainer: $(".step-end-game"),
    newGameSubmit: $(".new-game-submit"),
    playerCountInput: $("#game-player-count"),
    rollButton: $(".game-roll"),
    nextButton: $(".game-next"),
    currentPlayerNoText: $(".game-current-player-no span"),
    currentPlayerRollText: $(".game-current-player-roll span"),
    endGamePlayerNoText: $(".step-end-game span"),
  };

  return elements;
};

$(function async() {
  // all game state in a single object
  const state = {
    step: CONSTANTS.GAME_STEPS.NEW_GAME,
    playerCount: null,
    ladders: [],
    snakes: [],
    winnerPlayerNo: null,
    playerPositions: {},
    currentPlayer: {
      playerNo: 0,
      roll: false,
      position: 0,
    },
  };

  // whole game initialization
  const elements = getElements();
  const canvas = new fabric.StaticCanvas("game-canvas");
  const _game = game(elements, canvas, state);

  // input handlers (player count, roll button, next button)
  elements.playerCountInput.on("keyup", (e) => {
    e.preventDefault();

    const playerCount = parseInt(e.target.value);
    if (playerCount < 2 || playerCount > 4) {
      elements.playerCountInput.val("");
      state.playerCount = null;

      alert("Invalid number of players. Between 2 and 4 players allowed");

      return;
    }

    state.playerCount = parseInt(e.target.value);
  });

  elements.newGameSubmit.on("click", async (e) => {
    e.preventDefault();

    if (!state.playerCount || state.playerCount < 2 || state.playerCount > 4) {
      alert("Invalid number of players. Between 2 and 4 players allowed");
      return;
    }

    try {
      await _game.newGame(state.playerCount);
      _game.start();
    } catch (e) {
      console.error(e);
      alert("Could not start new game");
      return;
    }
  });

  elements.rollButton.on("click", async (e) => {
    e.preventDefault();

    try {
      await _game.roll();
    } catch (e) {
      console.error(e);
      alert("Could not roll dice");
      return;
    }
  });

  elements.nextButton.on("click", async (e) => {
    e.preventDefault();

    try {
      await _game.tick();
    } catch (e) {
      console.error(e);
      alert("Could not move to next player");
      return;
    }
  });
});

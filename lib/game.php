<?php

class Game
{
    public $playerCount = 0;
    public $player = array();
    public $board = array("size" => 0, "cells" => 0, "snakes" => array(), "ladders" => array());

    public $turn = array("player" => false, "roll" => false, "position" => 0);
    public $winningPlayer = null;

    function __construct()
    {
    }

    function build($_boardSize, $_playerCount)
    {
        $this->buildBoard($_boardSize);
        $this->buildPlayers($_playerCount);
        $this->turn["player"] = 0;
    }

    function buildBoard($_boardSize)
    {
        $this->board["size"] = $_boardSize;
        $this->board["cells"] = $_boardSize * $_boardSize;

        list($ladders, $snakes) = Game::getLaddersAndSnakes($this->board["cells"], $this->board["size"]);

        $this->board["ladders"] = array();
        for ($i = 0; $i < count($ladders); $i++) {
            $ladder = $ladders[$i];
            list($start, $end) = $ladder;
            $this->board["ladders"][$start] = $end;
        }

        $this->board["snakes"] = array();
        for ($i = 0; $i < count($snakes); $i++) {
            $snake = $snakes[$i];
            list($start, $end) = $snake;
            $this->board["snakes"][$start] = $end;
        }
    }

    function buildPlayers($_playerCount)
    {
        $this->playerCount = $_playerCount;
        for ($i = 0; $i < $_playerCount; $i++) {
            $this->player[$i] = array("position" => 0);
        }
    }


    static function getLaddersAndSnakes($boardCells, $boardSize)
    {
        // brute force it
        $snake1 = Game::getSnake($boardCells, $boardSize);
        $snake2 = Game::getSnake($boardCells, $boardSize);

        $ladder1 = Game::getLadder($boardCells, $boardSize);
        $ladder2 = Game::getLadder($boardCells, $boardSize);
        $ladder3 = Game::getLadder($boardCells, $boardSize);

        // make sure there is no ladder that starts from the same place a snake starts
        $starts = array($snake1[0], $snake2[0], $ladder1[0], $ladder2[0], $ladder3[0]);
        if (array_has_dupes($starts)) return Game::getLaddersAndSnakes($boardCells, $boardSize);

        // make sure there is no ladder that ends on a snake start or viceversa
        $ends1 = array($snake1[1], $snake2[1], $ladder1[0], $ladder2[0], $ladder3[0]);
        $ends2 = array($snake1[0], $snake2[0], $ladder1[1], $ladder2[1], $ladder3[1]);
        if (array_has_dupes($ends1) || array_has_dupes($ends2)) return Game::getLaddersAndSnakes($boardCells, $boardSize);

        return [[$ladder1, $ladder2, $ladder3], [$snake1, $snake2]];
    }

    public function tick()
    {
        // game has finished
        if ($this->winningPlayer != null) {
            $this->turn["position"] = false;
            $this->turn["roll"] = false;
            return;
        };

        // only move to next player if there has been a roll previously (meaning a positon for the player has been set)
        if ($this->turn["position"] == false) return false;

        // move to next player
        if ($this->turn["player"] < $this->playerCount - 1) {
            $this->turn["player"] += 1;
        } else {
            $this->turn["player"] = 0;
        }


        // reset next player's state
        $this->turn["position"] = false;
        $this->turn["roll"] = false;
    }

    public function roll()
    {
        // game has finished
        if ($this->winningPlayer != null) {
            $this->turn["position"] = false;
            $this->turn["roll"] = false;
            return;
        };

        $this->turn["roll"] = Game::getRoll();
        $position = $this->player[$this->turn["player"]]["position"] + $this->turn["roll"];
        if (array_key_exists($position, $this->board["ladders"])) {
            $position = $this->board["ladders"][$position];
        } elseif (array_key_exists($position, $this->board["snakes"])) {
            $position = $this->board["snakes"][$position];
        }
        if ($position > $this->board["cells"] - 1) return;

        $this->turn["position"] = $position;

        // save previous player position
        $this->player[$this->turn["player"]]["position"] = $this->turn["position"];

        // end game;
        if ($this->turn["position"] == $this->board["cells"] - 1) {
            $this->winningPlayer = $this->turn["player"];
        }
    }

    static function getLadder($boardCells, $boardSize)
    {
        $span = Game::getSpan($boardCells, $boardSize);
        if ($span[1] > $span[0]) return $span;
        return Game::getLadder($boardCells, $boardSize);
    }


    static function getSnake($boardCells, $boardSize)
    {
        $span = Game::getSpan($boardCells, $boardSize);
        if ($span[0] > $span[1]) return $span;
        return Game::getSnake($boardCells, $boardSize);
    }

    static function getSpan($boardCells, $boardSize)
    {
        // prevent starting from the first cell and prevent ending on the last cell
        $start = rand(1, $boardCells - 2);
        $end = rand(1, $boardCells - 2);

        // determine the row starting and ending cell for the span starting cell
        $cellsToRowStart = $start % $boardSize - 1;
        $rowStart = $start - $cellsToRowStart - 1;
        $rowEnd = $rowStart + $boardSize;

        // make sure that the span doesnt start and end in the same cell and also that they are not on the same row
        if ($start != $end && ($end > $rowEnd || $end < $rowStart)) return [$start, $end];
        return Game::getSpan($boardCells, $boardSize);
    }

    static function getRoll()
    {
        // dice numbers
        return rand(1, 6);
    }

    function getState()
    {
        // for serializing the state for storage
        return array("board" => $this->board, "player" => $this->player, "turn" => $this->turn, "playerCount" => $this->playerCount, "winningPlayer" => $this->winningPlayer);
    }
}


function array_has_dupes($array)
{
    return count($array) !== count(array_unique($array));
}
